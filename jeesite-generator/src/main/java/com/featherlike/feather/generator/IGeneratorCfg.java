/*****************************************************************
 *仿佛兮若轻云之蔽月，飘飘兮若流风之回雪
 *@filename IGeneratorCfg.java
 *@author WYY
 *@date 2013年11月11日
 *@copyright (c) 2013  wyyft@163.com All rights reserved.
 *****************************************************************/
package com.featherlike.feather.generator;

public interface IGeneratorCfg {
	String getModuleName();

	String getPackageName();

	String getFunctionName();

	String getTemplatePath();

	String getExcelPath();
}
